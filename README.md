# gradle-simple

[![](https://jitpack.io/v/org.bitbucket.punicappinternal/sncore.svg)](https://jitpack.io/#org.bitbucket.punicappinternal/sncore)

Punicapp social networks core library, which is used in internal projects.

To install the library add: 
 
```
   allprojects {
       repositories {
           maven { url 'https://jitpack.io' }
       }
   }
   dependencies {
           compile 'org.bitbucket.punicappinternal:sncore:1.0.1'
   }
```