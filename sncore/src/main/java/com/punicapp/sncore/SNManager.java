package com.punicapp.sncore;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by ARTem on 05.02.2018.
 */

public class SNManager {

    private Map<SNType, ISNConnector> map;


    private Context context;

    SNManager(Map<SNType, ISNConnector> map, Context context) {
        this.map = map;
        this.context = context;

    }

    public Observable<SNAccessToken> auth(SNType type) {
        PublishSubject<SNAccessToken> processor = PublishSubject.create();
        ISNConnector connector = map.get(type);
        connector.bindAuthCallback(processor);

        Intent intent = new Intent(context, ShadowActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        Bundle bundle = new Bundle();
        bundle.putBinder(ShadowActivity.SOC_PARAM, new ConnectorWrapper(connector));
        intent.putExtras(bundle);
        context.startActivity(intent);

        return processor;

    }


    public static class Builder {

        private Map<SNType, ISNConnector> map = new HashMap<>();

        private Context context;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder with(ISNConnector connector) {
            map.put(connector.getType(), connector);
            return this;
        }

        public SNManager build() {
            return new SNManager(map, context);
        }

    }

}
