package com.punicapp.sncore;


public class SNAccessToken {
    //*** Access token string for social network*/
    private String token;
    //*** Access secret string for social network if present*/
    private String secret;
    //*** Access userId string for social network if present*/
    private String userId;

    public SNAccessToken(String token, String secret, String userId) {
        this.token = token;
        this.secret = secret;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "SNAccessToken{\n" +
                "token='" + token + "\n'" +
                ", secret='" + secret + "\n'" +
                ", userId='" + userId + "\n'" +
                '}';
    }

    public String getSecret() {
        return secret;
    }

    public String getToken() {
        return token;
    }

    public String getUserId() {
        return userId;
    }
}