package com.punicapp.sncore;

import android.app.Activity;
import android.content.Intent;

import io.reactivex.subjects.PublishSubject;

/**
 * Created by ARTem on 05.02.2018.
 */

public interface ISNConnector {

    SNType getType();

    boolean requestAuth(Activity shadowActivity);

    void handleActivityResult(int requestCode, int resultCode, Intent data);

    void bindAuthCallback(PublishSubject<SNAccessToken> observer);
}
