package com.punicapp.sncore;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

@TargetApi(Build.VERSION_CODES.M)
public class ShadowActivity extends Activity {
    public static final String SOC_PARAM = "punicapp.soc.connector";
    private ISNConnector socialNetwork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }

        if (savedInstanceState == null) {
            handleIntent(intent);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        ConnectorWrapper binder = (ConnectorWrapper) intent.getExtras().getBinder(SOC_PARAM);
        if (binder == null) {
            finish();
            return;
        }
        socialNetwork = binder.getData();
        if (socialNetwork == null) {
            finish();
            return;
        }

        boolean flag = socialNetwork.requestAuth(this);
        if (flag) {
            finish();
            return;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (socialNetwork != null) {
            socialNetwork.handleActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
        finish();
    }
}
