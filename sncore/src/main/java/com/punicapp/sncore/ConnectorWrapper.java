package com.punicapp.sncore;

import android.os.Binder;

/**
 * Created by Elena on 05.02.2018.
 */

public class ConnectorWrapper extends Binder {

    private final ISNConnector data;

    public ConnectorWrapper(ISNConnector data) {
        this.data = data;
    }

    public ISNConnector getData() {
        return data;
    }
}
