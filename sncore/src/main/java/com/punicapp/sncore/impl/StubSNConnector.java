package com.punicapp.sncore.impl;

import android.app.Activity;
import android.content.Intent;

import com.punicapp.sncore.ISNConnector;
import com.punicapp.sncore.SNAccessToken;
import com.punicapp.sncore.SNType;

import io.reactivex.Observer;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by ARTem on 05.02.2018.
 */

public class StubSNConnector implements ISNConnector {

    private Observer<SNAccessToken> observer;

    @Override
    public SNType getType() {
        return SNType.STUB;
    }

    protected boolean isLoggedIn() {
        return true;
    }

    @Override
    public boolean requestAuth(Activity shadowActivity) {
        observer.onNext(new SNAccessToken("AAAA", "AAAAA", "AAAAA"));
        return true;
    }

    @Override
    public void handleActivityResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public void bindAuthCallback(PublishSubject<SNAccessToken> observer) {
        this.observer = observer;
    }
}
